# Selling My Stuff
In the summer of 2015, I was moving out of Canada. I made this small app to list all the things I had to sell. 

I found sites like Kijiji (Canada's "Craig's list" / "Gumtree") lacked features when it came to managing so many items.

So I made this one-page using Knockout.js, that I had discovered at work.

## Live demo

https://slabruyere.net/apps/selling-my-stuff

## Run the page

Open a Terminal and follow those few steps.
```bash
# 1- navigate to your workspace
cd ~/workspace 
# 2- clone the repository
git clone git@gitlab.com:slabruyere/selling-my-stuff.git
# 3- navigate to the project's directory
cd selling-my-stuff
# run a server of your choice
http-server -c-1 -p 8080 
```

Then open a browser and navigate to `localhost:8080` to see the app running.

## How to change the text

Most of the text (description, contact information) can be changed in `index.html`.

The rest (title, subtitle, labels, …) is in `viewmodel.js`.

## How to change the items

### Add the pictures in `items/`

First of all, create a new folder containing the pictures you want to display in the `items/` directory for each of the items you're selling, e.g.:

```bash
mkdir items/stuff-im-selling
mv ~/Pictures/stuff/that-thing-im-selling.jpg items/stuff-im-selling
```

### Add the item in `viewmodel.js`

Open `viewmodel.js` in your IDE and head to the definition of the `items` variable. It is an array containing… the items you're selling. Each item has the following structure:

```javascript
{
			Key: "stuff-im-selling", // should be unique
			Name: "Stuff I'm Selling", // the name that is displayed on the page
			Availability: null, // any string detailing a specific availability
			Price: "15", // a $ sign is prepended automatically
			Type: "Book", // categorize your items to help for search
			Description: "<p>Barely used</p>", // write your description in HTML
			AdditionalDescription: null, // idem id you need to add further details
			Icon: "stuff-im-selling/that-thing-im-selling.jpg", // the main picture for your item
			Images: [{
				src:"stuff-im-selling/side.jpg", // each additional image must have a source
				caption:"Side view" // the caption is optional, though
			},{
				src:"stuff-im-selling/top.jpg",
				caption:"Top view"
			}],
			showMoreInfo: ko.observable(false), // don't worry about this
			showPictures: ko.observable(false) // don't worry about this
}
```

You should of course get rid of the `this` and `that` items as you add yours.

## How to run the app on your server

Well, just put the whole `selling-my-stuff` folder on your server :)
Actually, there's one additional trick so that `require.js` gets all the dependencies correctly: in `viewmodel.js`, make sure the `require.config.baseUrl` property points to the path of `selling-my-stuff` on your server! For instance, on the live demo, it is set to `apps/selling-my-stuff`.

## Comments

There are many things I could improve in this small project but as is, it works and can be helpful so please use it as much as you want!
