define(["knockout"], function (ko) {
	ko.bindingHandlers['href'] = {
		update: function (element, valueAccessor) {
			ko.bindingHandlers.attr.update(element, function () {
				return { href: valueAccessor()}
			});
		}
	};

	ko.bindingHandlers['src'] = {
		update: function (element, valueAccessor) {
			ko.bindingHandlers.attr.update(element, function () {
				return { src: valueAccessor()}
			});
		}
	};

	ko.bindingHandlers['title'] = {
		update: function (element, valueAccessor) {
			ko.bindingHandlers.attr.update(element, function () {
				return { title: valueAccessor()}
			});
		}
	};

	ko.bindingHandlers['toggle'] = {
		init: function (element, valueAccessor) {
			var value = valueAccessor();
			ko.applyBindingsToNode(element, {
				click: function () {
					value(!value());
				}
			});
		}
	};

	ko.bindingHandlers['currency'] = {
		symbol: ko.observable('$'),
		update: function (element, valueAccessor, allBindingsAccessor){
			return ko.bindingHandlers.text.update(element,function (){
				var value = +(ko.utils.unwrapObservable(valueAccessor()) || 0),
					symbol = ko.utils.unwrapObservable(allBindingsAccessor().symbol === undefined
								? allBindingsAccessor().symbol
								: ko.bindingHandlers.currency.symbol);
				return symbol + value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			});
		}
	};

	ko.bindingHandlers.select2 = (function () {
		var actionsEvents = {
			'focus': 'select2-blur',
			'open': 'select2-close',
			'close': 'select2-open',
		};

		return {
			init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
				ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
					$(element).select2('destroy');
				});
			},
			update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
				// need to reinit select2 if options or value has changed to handle valueAllowUnset
				var $element = $(element);
				var value = ko.utils.unwrapObservable(allBindings.get('value'));
				var selectedOptions = ko.utils.unwrapObservable(allBindings.get('selectedOptions'));
				var options = ko.utils.unwrapObservable(allBindings.get('options'));
				var settings = ko.toJS(valueAccessor());
				$element.select2(settings);
				$element.select2('val', value || selectedOptions);
				
				Object.keys(actionsEvents).forEach(function (a) {
					if(settings[a]) {
						$element.select2(a);
						$element.on(actionsEvents[a], function () {
							var b = valueAccessor()[a];
							if(!ko.isComputed(b)) { 
								if(ko.isObservable(b)) {
									b(false);
								} else {
									b = false;
								}
							}
						});
					}
				});
			}
		};
	}());

	ko.bindingHandlers['collapse'] = {
		init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
			var $element = $(element);
			var rootModel = bindingContext.$root
			var options = valueAccessor();

			if (options.toggle) {
				if(ko.utils.unwrapObservable(options.toggle)) {
					$element.addClass("collapse");
				} else {
					$element.addClass("in");
				}
				options.toggle.subscribe(function (value) {
					$element.collapse(value ? 'show' : 'hide');
				});
			}

			ko.computed(function () {
				if(options.parent) {
					options.parent = ko.utils.unwrapObservable(options.parent);
				}
				$element.collapse(options);
			}, null, {'disposeWhenNodeIsRemoved': element});
		}
	};

	ko.bindingHandlers['popover'] = {
		init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
			var $element = $(element);
			var rootModel = bindingContext.$root
			var options = valueAccessor();

			var makePopover = function (options) {
				$element.popover('destroy').popover(options);
				$element.on('shown.bs.popover', function () {
					var $tip = $element.data('bs.popover').$tip;
					ko.cleanNode($tip[0]);
					ko.applyBindings(options.viewModel, $tip[0]);
				});
			};

			ko.computed(function () {
				options.title = ko.utils.unwrapObservable(options.title);
				options.content = ko.utils.unwrapObservable(options.content);
				options.placement = ko.utils.unwrapObservable(options.placement);
				options.container = ko.utils.unwrapObservable(options.container);
				options.trigger = options.toggle ? 'manual' : options.trigger;
				options.viewModel = options.viewModel || viewModel;

				if (options.container) {
					ko.bindingHandlers.element.getModelElement(rootModel, options.container, function (element) {
						options.container = element;
						makePopover(options);
					});
				} else {
					makePopover(options);
				}
			}, null, {'disposeWhenNodeIsRemoved': element});

			if (options.toggle) {
				options.toggle.subscribe(function (value) {
					$element.popover(value ? 'show' : 'hide');                                                                         
				});
			}
		}
	};
});