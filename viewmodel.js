define(["knockout"], function (ko) {
	return function () {
		var viewModel = {};

		// isLoading
		viewModel.isLoading = ko.observable(false);

		// title
		viewModel.title = "Selling My Stuff"
		viewModel.subtitle = "Everything must go!";

		// sorts
		viewModel.sorts = [
			"Name A->Z",
			"Name Z->A",
			"Price low->high",
			"Price high->low",
		];

		var getRightSort = function (value) {
			if(value === "Name A->Z") {
				return sortByAscName;
			}
			if(value === "Name Z->A") {
				return sortByDescName;
			}
			if(value === "Price low->high") {
				return sortByAscPrice;
			}
			if(value === "Price high->low") {
				return sortByDescPrice;
			}
		};

		viewModel.selectedSort = ko.observable();
		viewModel.selectedSort.subscribe(function (value) {
			viewModel.items(viewModel.items().sort(getRightSort(value)));
		});

		var sortByAscName = function (a, b) {
			return a.Name.toLowerCase() > b.Name.toLowerCase() ? 1 : -1;
		};
		var sortByDescName = function (a, b) {
			return b.Name.toLowerCase() > a.Name.toLowerCase() ? 1 : -1;
		};
		var sortByAscPrice = function (a, b) {
			return formatNumber(a.Price) - formatNumber(b.Price);
		};
		var sortByDescPrice = function (a, b) {
			return formatNumber(b.Price) - formatNumber(a.Price);
		};

		var formatNumber = function (str) {
			return parseFloat(str.replace(/[^0-9-.]/g, ""));
		};

		// items
		var items = [{
			Key: "this",
			Name: "This item",
			Price: "50",
			Availability: "Available next week",
			Type: "Tool",
			Description: [
				"<p>A great description.</p>",
				"<p>Something also great about this item</p>"
				].join("\r\n"),
			AdditionalDescription: [
				"<p>Here are some additional details</p>",
				"<ul>",
					"<li>1st detail</li>",
					"<li>2nd detail</li>",
				"</ul>"
			].join("\r\n"),
			Icon: "this/icon.jpg",
			Images: [{
				src: "this/side.jpg",
				caption: "Side"
			},{
				src: "this/front.jpg",
				caption: "Front"
			}],
			showMoreInfo: ko.observable(false),
			showPictures: ko.observable(false)
		},{
			Key: "that",
			Name: "That item",
			Availability: null,
			Price: "15",
			Type: "Book",
			Description: "<p>Small item.</p>",
			AdditionalDescription: null,
			Icon: "that/icon.jpg",
			Images: [{
				src:"that/icon.jpg",
				caption:"That"
			},{
				src:"that/side.jpg",
				caption:"Side view"
			}],
			showMoreInfo: ko.observable(false),
			showPictures: ko.observable(false)
		}];

		viewModel.items = ko.observableArray(items);

		viewModel.types = viewModel.items().map(function (it) {
			return it.Type;
		}).filter(function (e, i, arr) {
			return arr.lastIndexOf(e) === i;
		}).sort();

		viewModel.selectedTypes = ko.observableArray();
		viewModel.searchField = ko.observable("");

		viewModel.search = function () {
			var allTypes = viewModel.selectedTypes().length === 0;
			var allContent = viewModel.searchField().trim().length === 0;
			if(allTypes && allContent) {
				viewModel.items(items.sort(getRightSort(viewModel.selectedSort())));
			} else {
				if(!allContent) {
					var re = new RegExp("" + viewModel.searchField().trim().toLowerCase(), "i");
				}
				viewModel.items(items.filter(function (it) {
					return (allTypes || viewModel.selectedTypes().indexOf(it.Type) > -1) &&
						(allContent || re.test(it.Name) || re.test(it.Description) || re.test(it.AdditionalDescription) || re.test(it.Availability));
				}).sort(getRightSort(viewModel.selectedSort())));
			}
		};

		viewModel.selectedTypes.subscribe(function (value) {
			viewModel.search();
		});

		viewModel.handleKeypress = function (d, e) {
			var key = e.keyCode || e.which;
			if(key == 13) {
				e.target.blur();
				viewModel.search();
			}
			return true;
		};

		viewModel.toggleMoreInfo = function (item) {
			item.showMoreInfo(!item.showMoreInfo());
		};

		viewModel.togglePitcures = function (item) {
			item.showPictures(!item.showPictures());
		};

		return viewModel;
	};
});
